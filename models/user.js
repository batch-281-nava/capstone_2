const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
	email: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderedProduct: [{
		products: [{
			productId: {
				type: String
			},
			productName: {
				type: String
			},
			quantity: {
				type: Number
			}
		}],
		totalAmount: {
			type: Number
		},
		purchasedOn: {
			type: Date,
			defaultdefault: new Date()
		}
	}]
})

userSchema.statics.login = async function(email, password) {
	if(!email || !password) {
		throw Error('Please fill up email and password.')
	}

	const user = await this.findOne({ email })

	if(!user || user.password !== password) {
		throw Error('Email or Password is incorrect.')
	}

	return user
}

userSchema.statics.register = async function(email, password, isAdmin) {
	if(!email || !password) {
		throw Error('Please fill up email and password.')
	}

	const userExist = await this.findOne({ email })

	if(userExist) {
		throw Error('Email is already taken.')
	}

	if(isAdmin !== undefined) {
		const userAdmin = await this.create({ email, password, isAdmin })
		return userAdmin

	} else {
		const user = await this.create({ email, password })
		return user
	}

} 


module.exports = mongoose.model('User', userSchema)