const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	userOrders: [{
		userId: {
			type: String,
			required: true,
			// ref: 'User'
		},

	}]
}, { timestamps: true })


module.exports = mongoose.model('Product', productSchema)