const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')
require('dotenv').config()

const port = process.env.PORT || 5000
mongoose.connect(process.env.MONGODB)
	.then(() => {
		app.listen(port, () => {
			console.log(`Server is listening on port ${port}`)
		})
	})
	.catch((error) => console.log(error.message))

const productRoutes = require('./routes/product')
const userRoutes = require('./routes/user')

app.use(cors())
app.use(express.json())
app.use((req, res, next) => {
	console.log(`${req.method} - ${req.path}`)
	next()
})

app.use('/api/products', productRoutes)
app.use('/api/user', userRoutes)

