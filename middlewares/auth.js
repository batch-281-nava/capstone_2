const User = require('../models/user')
const jwt = require('jsonwebtoken')

const authenticateUser = async(req, res, next) => {
	const { authorization } = req.headers
	if(!authorization) {
		return res.status(401).json({message: 'Authorization token is required.'})
	}

	const token = authorization.split(' ')[1]

	try {
		const { _id } = jwt.verify(token, process.env.SECRET_CODE)
		req.user = await User.findOne({_id}).select('_id')
		next()
	} catch (error) {
		res.status(401).json({message: 'Request token is invalid.'})
	}
}

const validateUser = async(req, res, next) => {
	const { _id } = req.user

	try {
		const user = await User.findById(_id)
		if(!user.isAdmin) {
			throw Error('Only admin can manipulate product details.')
		}
		next()
	} catch (error) {
		return res.status(401).json({message: error.message})
	}
}

module.exports = {
	authenticateUser,
	validateUser
}