const Product = require('../models/product')

const getAllProducts = async(req, res) => {
	try {
		const products = await Product.find({isActive: true}).sort({createdAt: -1})
		res.status(200).json(products)
	} catch (error) {
		res.status(400).json({message: error.message})
	}
}

const addNewProduct = async(req, res) => {
	const { productName, productDescription, productPrice, userId } = req.body

	try {
		const product = await Product.create({
			name: productName,
			description: productDescription,
			price: productPrice
		})

		res.status(200).json({message: 'Successfully added new product', product})
	} catch (error) {
		res.status(400).json({message: error.message})
	}
}

const getProduct = async(req, res) => {
	const { productId } = req.params

	try {
		const product = await Product.findById(productId)
		if(!product) {
			throw Error('Product not found.')
		}
		res.status(200).json(product)
	} catch (error) {
		res.status(404).json({message: error.message})
	}
}

const updateProduct = async(req, res) => {
	const { productId } = req.params
	const { productName, productDescription, productPrice, isActive } = req.body 

	const active = isActive ? true : false

	try {
		const product = await Product.findByIdAndUpdate({_id: productId}, {
			name: productName,
			description: productDescription,
			price: productPrice,
			isActive: active
		}, {new: true})
		
		if(!product) {
			throw Error('Cannot update nonexistent product.')
		}
		res.status(200).json(product)
	} catch (error) {
		res.status(400).json({message: error.message})
	}
}

const deleteProduct = async(req, res) => {
	const { productId } = req.params

	try {
		const product = await Product.findByIdAndDelete(productId)
		if(!product) {
			throw Error('Cannot delete nonexistent product.')
		}
		res.status(200).json({message: 'Successfully deleted product in database'})
	} catch (error) {
		res.status(400).json({message: error.message})
	}
}

module.exports = {
	getAllProducts,
	addNewProduct,
	getProduct,
	updateProduct,
	deleteProduct
}