const User = require('../models/user')
const jwt = require('jsonwebtoken')

const createToken = (_id) => {
	return jwt.sign({_id}, process.env.SECRET_CODE, {expiresIn: '1d'})
}

const login = async(req, res) => {
	const { email, password } = req.body
	try {
		const user = await User.login(email, password)
		const token = createToken(user._id)
		res.status(201).json({user, token})
	} catch (error) {
		res.status(400).json({message: error.message})
	}
}

const register = async(req, res) => {
	const {email, password, isAdmin} = req.body
	// ITO YUNG API NATIN SA REGISTER
	// BACKEND
	console.log("USER INPUTS FROM FRONTEND", req.body)
	try {
		const user = await User.register(email, password, isAdmin)
		const token = await createToken(user._id)
		res.status(201).json({user, token})
	} catch(error) {
		res.status(400).json({message: error.message})
	}
}

module.exports = {
	login,
	register
}