const express = require('express')
const router = express.Router()

const { getAllProducts, addNewProduct, getProduct, updateProduct, deleteProduct } = require('../controllers/product')
const {authenticateUser, validateUser } = require('../middlewares/auth')

router.get('/', authenticateUser, getAllProducts)

router.post('/', authenticateUser, validateUser, addNewProduct)

router.get('/:productId', authenticateUser, getProduct)

router.put('/:productId', authenticateUser, validateUser, updateProduct)

router.delete('/:productId', authenticateUser, validateUser, deleteProduct)

module.exports = router